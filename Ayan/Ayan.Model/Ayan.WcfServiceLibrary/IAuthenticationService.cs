﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Ayan.WcfServiceLibrary
{
    [ServiceContract]
    public interface IAuthenticationService
    {
        [OperationContract]
        string GetName(UserInfo userinfo);
        [OperationContract]
        int Login(string UserName, string Password);
    }

    
}
