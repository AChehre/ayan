﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Ayan.WcfServiceLibrary
{
    interface IBaseEntity
    {
        
    }

    [DataContract]
     public class BaseEntity : IBaseEntity
    {
        [DataMember]
        public int Id { get; set; }
    }
}
