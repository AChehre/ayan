﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Ayan.WcfServiceLibrary
{
    [DataContract]
    public class UserInfo : BaseEntity
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public Boolean SysAdmin { get; set; }
    }
}
